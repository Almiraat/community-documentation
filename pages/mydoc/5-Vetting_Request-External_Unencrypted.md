---
title: Vetting Request - External Unencrypted
keywords: email templates, vetting, vetting process, partner
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "External Vetting, Email to Contact if unencrypted"
sidebar: mydoc_sidebar
permalink: 5-Vetting_Request-External_Unencrypted.html
folder: mydoc
conf: Public
lang: en
---


# Vetting Request - External Unencrypted
## External Vetting, Email to Contact if unencrypted

### Body

Hi [NAME],

My name is [IH's name] and I’m part of Access Now Digital Security Helpline. I received your contact information from [Name of Access Now Employee or partner (NOT the client name)]. I’m respectfully contacting you to ask for assistance with the vetting process of a new Helpline client.

As part of our procedures, we require for trusted contacts to verify the identity of this new client. To further communicate the details, we need to establish a secure channel. 

We can assist with the process of setting up PGP for encrypted emails or Signal for encrypted instant messaging. Please let us know if you need any help. 

Thanks in advance,
[IH's name]
