---
title: Initial Reply - For Non-Spanish Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Non-Spanish speakers"
sidebar: mydoc_sidebar
permalink: 221-Initial_Reply_For_Non-Spanish_Speakers.html
folder: mydoc
conf: Public
lang: es
---


# Initial Reply - For Non-Spanish Speakers
## First response, Email to Client if you're a non-Spanish speaker

### Body


Hola [Client Name]:

Mi nombre es [Your Name], soy parte del equipo de la Línea de Ayuda en Seguridad Digital de Access Now
<https: accessnow.org="" help="">
 .

Hemos recibido su mensaje [Email Subject]. Lastimosamente yo no hablo español fluido, si esta es una cuestión urgente, por favor responder a este mensaje agreagando la etiqueta “URGENT” en el asunto y le atenderemos lo mas pronto posible.

Si lo prefiere podemos mover la conversación a [Languages spoken by incident handler: English=Inglés, French=Francés, Portuguese=Portugués, Arabic=Árabe], también me encuentro disponible para chatear en la direccion [xxxx@accessnow.org].

Nuestros compañeros y nuestras compañeras hispanohablantes estarán en turno dentro de unas horas para darle seguimiento al caso.

Cordialmente,

[Your name]


* * *


### Related Articles

- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
</https:>