---
title: Vetting Request -  Encrypted
keywords: email templates, vetting, vetting process, partner
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "External Vetting, email to partner (only encrypted)"
sidebar: mydoc_sidebar
permalink: 10-Vetting_Request-Encrypted.html
folder: mydoc
conf: Public
lang: en
---


# Vetting Request -  Encrypted
## External Vetting, email to partner (only encrypted)

### Body

Dear [NAME],

My name is [IH's Name] and I’m part of Access Now Digital Security Helpline. I received your contact information from [Access Now Employee's or Partner's Name (Not the client information)]. I’m contacting you to ask for assistance with the vetting process of a new Helpline client.

Recently we received a help request from (XXXX name@domain, POSITION, ORGANIZATION). They are requesting our guidance for a digital security concern.

We would be thrilled to continue providing our help, but first need to verify with trusted partners their work and identity. It would be of great help for us if you could verify their identity and confirm they are a trustworthy member of civil society.

Could you please confirm that:

- You have met this individual in person more than once or have known them for an extended period of time and gained a sense of trustworthiness.
- You trust this individual to take actions that directly impact civil society in a positive manner.
- You believe others in this community can also trust this individual as someone who will take action only in positive non-harmful ways.

If you have any question or concern please let us know. In advance I appreciate your kind assistance. 

[YOUR NAME]
