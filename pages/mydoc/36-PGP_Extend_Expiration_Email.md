---
title: PGP - Extend Expiration Date - Email
keywords: email, PGP, key management, expiration date, Enigmail, Thunderbird, email security
last_updated: December 3, 2018
tags: [secure_communications_templates, templates]
summary: "Email to client explaining how to extend the expiration date of their PGP key"
sidebar: mydoc_sidebar
permalink: 36-PGP_Extend_Expiration_Email.html
folder: mydoc
conf: Public
lang: en
---


# PGP - Extend Expiration Date - Email
## Email to client to confirm that their new user ID has been added to their PGP key

### Body

Hi [Name], 

I'm writing to you because we have noticed that your PGP key will expire on [expiration date]. You will need to extend the expiration date of your key to make sure your email encryption keeps working.

Unless you have particular concerns and would like to proceed differently, I suggest you extend your key expiration for 5 more years.

To do this, you can follow these instructions:

1. In Thunderbird, click the options menu and select Enigmail -&gt; Key Management.

2. In the "Search for" box type your email address.

3. Right-click your PGP key and select "Change Expiration Date".

4.  Make sure both your primary key and your subkey are checked.

5. Enter "5 years" in the "Key expires in" field.

6. Click OK. A pop-up window will appear - enter the passphrase for your secret PGP key.

7. Right-click your PGP key and select "Upload Public Keys to Keyserver".

8. Reply to this email to confirm that you have extended the expiration date of your key and have uploaded the updated key to the key servers.

Once you have completed these steps, your updated key will be available on the key servers, so users can continue to send you encrypted emails without any issue.

If someone tells you they are having issues sending encrypted emails to you, ask them to refresh your key in their keyring. Feel free to also direct users to our Helpline in case they continue to have issues. 

Please let me know if you have any question or need any help.

Best,

[IH's Name]



* * *


### Related Articles

- [Article #37: PGP - Extend Expiration Date](37-PGP_Extend_Expiration.html)
