---
title: PGP - GnuPG V2 Warning on Linux Systems
keywords: email, PGP, gpg2, GnuPG, Linux, troubleshooting, email security
last_updated: November 7, 2018
tags: [secure_communications, articles]
summary: "A client is using Thunderbird + Enigmail on a Linux system. Enigmail is showing a warning message saying that the current version will be the last one to support GnuPG V1.X, or that GnuPG is not supported."
sidebar: mydoc_sidebar
permalink: 131-PGP_gpg2_Warning_Linux.html
folder: mydoc
conf: Public
lang: en
---


# PGP - GnuPG V2 Warning on Linux Systems
## A client is receiving warnings regarding the version of GnuPG installed in their computer

### Problem

- A warning message will appear every time the user opens Thunderbird. 
- The message does not explain how to solve the problem. 


* * *


### Solution

To solve this issue, the user needs to install the GnuPG2 package. The package is available in the official repositories all the main Linux distributions and the following steps should solve the issue:

1. Update the system and install gnupg2. This can be done through the package manager installed in the user's system or by command line, running the following command in the terminal:

        sudo apt-get update &amp; apt-get install gnupg2

2. Close all the applications related to Thunderbird and relaunch them. After this, if you go to Enigmail -&gt; About, it should say it's now using GnuPG2, and the warning message should not appear anymore.


* * *


### Comments



* * *


### Related Articles
