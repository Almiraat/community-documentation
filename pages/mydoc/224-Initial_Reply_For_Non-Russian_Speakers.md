---
title: Initial Reply - For Non-Russian Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Non-Russian speakers"
sidebar: mydoc_sidebar
permalink: 224-Initial_Reply_For_Non-Russian_Speakers.html
folder: mydoc
conf: Public
lang: ru
---


# Initial Reply - For Non-Russian Speakers
## First response, Email to Client if you're a non-Russian speaker

### Body


Уважаемая(ый) [Client Name],

Меня зовут [Your Name]. Я работаю в службе поддержки по вопросам цифровой безопасности организации Access Now
<https: accessnow.org="" help="">
 .

Ваш запрос ([Email Subject]) получен. К сожалению, я не говорю по-русски. Если вопрос срочный, пожалуйста ответьте на это письмо, добавив тэг «URGENT» в поле темы письма. Мы постараемся обработать ваш запрос как можно скорее. 

Если вам не составит труда продолжить переписку на английском языке, пожалуйста сообщите. Связаться со мной можно по адресу [xxxx@accessnow.org]. 

Наш русскоговорящий сотрудник будет в офисе в течение нескольких часов и ответит на ваше сообщение.

Всего доброго,

[Your name]


* * *


### Related Articles

- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
</https:>