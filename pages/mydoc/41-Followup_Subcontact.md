---
title: Follow-up - Subcontact
keywords: follow-up, referrals, third-party consultants, email templates
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "Email to client to follow up on a referral to a third party"
sidebar: mydoc_sidebar
permalink: 41-Followup_Subcontact.html
folder: mydoc
conf: Public
lang: en
---


# Follow-up - Subcontact
## Email to client to follow up on a referral to a third party

### Body

Hi [CLIENT'S NAME],

I'm [IH's NAME] from the Digital Security Helpline at Access Now.

Right now I'm doing a follow up on this case. Could you let us know if you succeeded in contacting [CONTACT'S NAME], the person who [add any details on what this person had to do]?

Thanks,

[IH's NAME]
