---
title: Post-DSC Arabic Outreach Template
keywords: email templates, outreach, events, DSC, digital security clinics
last_updated: July 30, 2018
tags: [helpline_procedures_templates, templates]
summary: "Arabic email template for outreach - addressed at members of civil society we met during a Digital Security Clinic or an event"
sidebar: mydoc_sidebar
permalink: 287-Outreach_Template_Arabic.html
folder: mydoc
conf: Public
lang: ar
---


# Post-DSC Arabic Outreach Template
## Email template for outreach - addressed at Arabic speaking members of civil society we met during a Digital Security Clinic or an event

### Body


عزيزي [Client Name] 


هذا [your Name] من منظمة أكسس ناو. لقد كان من دواعي سروري لقاؤك خلال 
[Event Name/DSC]


أود من خلال هذه الرسالة التعبير عن مدى استمتاعي بالتحدث إليك أثناء ذلك اللقاء. فريق السلامة المعلوماتية متجند خلال كامل ساعات اليوم على مدى كامل الأسبوع لتوفير المساعدة التقنية والنظرية في مايخص الحماية الرقمية أو التصدي للهجمات السيبرانية. يمكن لك أن تقرأ أكثر عن خدماتنا من خلال الرابط التالي:

https://www.accessnow.org/%D9%85%D8%B3%D8%A7%D8%B9%D8%AF%D9%88-%D8%A7%D9%84%D8%A3%D9%85%D8%A7%D9%86-%D8%A7%D9%84%D8%B1%D9%82%D9%85%D9%8A/?ignorelocale


كما تجد مفتاح التشفير 
PGP
 مرفق لهذه الرسالة. 

التشفير يساعد على حماية المحادثات بالكامل وبصفة مستقلة عن مزود الخدمة. سوف نكون سعداء بمساعدتك في تنصيب هذه التقنية. 


كما عبرت عن ذلك سلفاً، سوف نكون جاهزين لمساعدتك متى إقتضت الحاجة. 


شكراً،  

[your Name]


* * *

### Related Articles

- [Article #142: Outreach Template](142-Outreach_Template.html)
