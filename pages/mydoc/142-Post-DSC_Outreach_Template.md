---
title: Post-DSC Outreach Template
keywords: email templates, outreach, events, DSC, digital security clinics
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "Email template for outreach - addressed at members of civil society we met during a Digital Security Clinic or an event"
sidebar: mydoc_sidebar
permalink: 142-Post-DSC_Outreach_Template.html
folder: mydoc
conf: Public
lang: en
---


# Post-DSC Outreach Template
## Email template for outreach - addressed at members of civil society we met during a Digital Security Clinic or an event

### Body

Hi [Client Name],

This is [Your Name] from the Access Now Digital Security Helpline. It was great meeting you at [DSC/Event name].

I am just sending this email to let you know how much I enjoyed our conversation. Our helpline is available 24 hours a day every day of the year to provide any digital security advice or technical assistance you may need. You can read more about the helpline at https://www.accessnow.org/help and you can always contact us at help@accessnow.org.

I'm attaching our PGP public key. If you are not using PGP yet, we are
also available to assist you with the configuration, or can recommend other secure communications tools. It's always good to have a secure channel ready, in case we need it in the future.

As before, I remain available for any question or assistance.

Thanks,

[your Name]
