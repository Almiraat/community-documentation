---
title: Inform Clients of Vetting Process (French)
keywords: email templates, vetting, vetting process
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "Template to inform clients about our vetting process"
sidebar: mydoc_sidebar
permalink: 198-Vetting_Info_Clients_French.html
folder: mydoc
conf: Public
lang: fr
---


# Inform Clients of Vetting Process (French)
## Template to inform clients about our vetting process

### Body

Cher/Chère [ClientName],

Nous apprécions la confiance que vous mettez en la Helpline. Afin de protéger les utilisateurs confrontés aux difficultés, que nous aidons, et afin d'étendre notre réseau de connaissance et de confiance, nous procédons à une verification de tous nos clients. 

Au cours de ce procédé, nous contactons les partenaires de confiance d'AccessNow et nous partageons votre nom ainsi que votre adresse email, afin de confirmer votre appartenance à la "société civile". Toute autre information restera strictement confidentielle, y compris la raison pour laquelle vous nous avez contacté.

Nous continuerons entre temps à travailler sur votre problème (ticket #[IDofTheParentCase]) et restons disponibles pour vos questions. 

Bien à vous,

L'équipe Helpline.
