---
title: Handling of Fake Domains with Commercial Involvement
keywords: fake domain, out of mandate, commercial
last_updated: November 8, 2018
tags: [fake_domain_mitigation, articles]
summary: "A fake domain with commercial involvement has been reported. After the vetting process, we have found out the client is not part of civil society but is a commercial entity."
sidebar: mydoc_sidebar
permalink: 34-Handling_Fake_Domains_Commercial.html
folder: mydoc
conf: Public
lang: en
---


# Handling of Fake Domains with Commercial Involvement
## What to do if a fake domain with commercial involvement is reported

### Problem

Access Now Helpline, as a responsible CERT, is willing to responsibly report fake domain cases with commercial involvement. However, commercial entities are not under Access Now's mandate, and therefore the amount of resources to handle this type of cases should be as limited as possible.


* * *


### Solution

- If the domain is a country-code top level domain, contact the local CERT.

- If the domain is a generic top level domain, contact the local CERT of the country where the commercial entity is registered.

- If there is no local CERT, report the malicious activity directly to the registrar responsible for the domain.

A list of CERTs per country can be found [here](http://first.org/members/map).


* * *


### Comments



* * *

### Related Articles
