---
title: Warning on Possible Spear-Phishing Attacks
keywords: email templates, phishing, spearphishing, intermediaries, scamming, malware
last_updated: July 20, 2018
tags: [vulnerabilities_malware_templates, templates]
summary: "Template for writing to intermediaries to gather information on a possibly targeted phishing attack"
sidebar: mydoc_sidebar
permalink: 280-spear-phishing-evidence-gathering.html
folder: mydoc
conf: Public
lang: en
---


# Warning on Possible Spear-Phishing Attacks
## Template for writing to intermediaries to gather information on a possibly targeted phishing attack


### Body

Hi [$Partner_name],

We hope this email finds you well.

Our Access Now Helpline Team has noticed a worrying attack trend that has been
targeting [group of users] lately. We are worried about the privacy and
security of our users that we got to communicate with through you, and for this
reason we are reaching out to inform you of these attacks. Besides warning you,
we would like to know if you have faced or seen this type of incidents within
your environment and network, and of course provide our support and assistance
at any moment.

This attack is a phishing attack targeting [group of users], and it is based on
[vulnerability or feature the attack exploits]. 

Here’s how this attack works: [description of the attack].

That is the basic summary. [any other details]

Please do not hesitate to get back to us to report an ongoing incident of this
type or of an already resolved incident. 

In particular, it would be helpful if you could add the following details:

- **For email-based attacks**: The header of the email used in the phishing attack.
- The body of the message.
- Any other detail that can confirm or add to the above description of the
  attack.
- To perform a full analysis of the email, it would be most useful to view the full headers of the message. In this link you can find detailed instructions on how to obtain the
  email headers: https://www.circl.lu/pub/tr-07/, for different mail clients. If
  you need any help, please let me know.

Your input is highly appreciated, as we are trying to analyse all of these
hacking attempts and incidents to help us better warn our users and provide them
with recommendations to prevent these attacks.

Best Regards,

Access Now Helpline Team


* * *


### Related Articles

- [Article #281: How to Recognize Spear-Phishing and What to Do](281-spear-phishing.html)
