---
title: Secure Chat Tools for mobile devices
keywords: chat, IM, instant messaging, encryption, end-to-end encryption, mobile, Android, iOS, iPhone
last_updated: August 24, 2018
tags: [secure_communications, articles]
summary: "A client has requested recommendations for secure chat tools which are compatible with Android and iOS devices and have group chat support."
sidebar: mydoc_sidebar
permalink: 141-Secure_Chat_Mobile.html
folder: mydoc
conf: Public
lang: en
---


# Secure Chat Tools for mobile devices
## Recommendations on secure mobile chat tools for iOS and Android

### Problem

Chat tools for mobile devices are numerous. We should make sure that the secure chat tools we recommend meet all of the following requirements (or, if they don't, that their features meet the client's needs and match their threat model):

- Group Chat Support
- End-to-End Encryption
- Mature and maintained open source encryption protocol
- Possibly all components should be open source (client-side and server-side)
- Allows communication over mobile networks (3G/4G) or Wi-Fi
- Sends notifications
- Interoperability between iOS and Android


* * *


### Solution

There are many chat apps that are available for iOS and Android, but only a few meet all of the above requirements. To decide what tools to recommend, we should compare the features of each tool with the client's threat model and needs - although the preference should be for fully open source tools, sometimes the client might have other needs that are not met by the available free and open source software.


#### Free and open source centralized tools

##### Signal

Among all apps, Signal meets all of the above requirement and does not store metadata.

Signal is an Android, iOS and desktop app that offers end-to-end encrypted instant messaging, group chat, and 1:1 voice calls.

The encryption is based on the [Signal protocol](https://signal.org/docs/), and software is open source both client- and server-side.

Let the client know about relevant Signal Features:

-  End-to-end encryption for IM, group chat, voice messages, file sharing, and 1:1 voice calls
-  Ability to verify the identity of other users (using key or QR code)
-  Open source ([GitHub link](https://github.com/whispersystems))
-  Intuitive design
-  Also allows users to send regular SMS messages (on Android, unencrypted)
-  Allows message attachments
-  Notifications
-  Possibility of setting a timer for disappearing messages

Instructions for installing and using Signal:

- [Android](https://ssd.eff.org/en/module/how-use-signal-android)

    On Android devices, Signal can also be installed without the Play Store. This is recommended only if the client can verify the .apk file. The .apk file and its SHA256 fingerprint can be found in [this page](https://signal.org/android/apk/)
    
- [iOS](https://ssd.eff.org/en/module/how-use-signal-ios)

- Signal can be used through a desktop app, but it first needs to be registered on a phone. Once it's been registered, the client can download the desktop app [here](https://signal.org/download/), install it, and link it to their account. Instructions for linking an account to a desktop app can be found [here](https://support.signal.org/hc/en-us/articles/360007320551).

- [Signal's support pages](http://support.whispersystems.org/hc/en-us)
- [Signal's declaration on metadata](https://signal.org/bigbrother/eastern-virginia-grand-jury/)


###### Warnings and recommendations

- Signal can only be registered through an existing phone number, which becomes the user's identifier. If the client does not wish to use their personal phone number or to share it with their contacts, they can register Signal with a burner SIM card (if available in their country), or with an online phone service. To explain how to do this, we can find instructions in [this tutorial](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/).

- Signal can also be used for voice calls, but not through the desktop app.

- Signal is censored in Egypt, Qatar, and the United Arab Emirates. In other countries it cannot be registered, but can be used if registered abroad.



##### Wire

Wire is a cross-platform, encrypted instant messaging client available for iOS, Android, Linux, Windows, macOS and web browser clients. It can be used to make voice and video calls, and to send text messages and files.

It requires a phone number or email address for registration. If the client needs anonymity, they can create e new email account with the Tor Browser and use that account to register on Wire.

- [Instructions for registering a Wire account with an email address](https://support.wire.com/hc/en-us/articles/360000165369-How-can-I-register-with-an-email-)

Wire is free for personal use and offers additional group functionalities for paid accounts. For registering an account for personal use, clients should visit [this page](https://wire.com/en/download/)

Features:

-  End-to-end encryption for IM, group chat, voice messages, file sharing, voice calls, and 1:1 video calls
-  Ability to verify the identity of other users' devices
-  Open source ([code on GitHub](https://github.com/wireapp/wire))
-  Intuitive design
-  Allows message attachments and drawings
-  Notifications
-  Possibility of setting a timer for disappearing messages
-  Possibility of deleting messages in the recipient's devices
-  End-to-end encrypted group video calls (can only be initiated by a user with a paid account)
-  [Secure guest rooms](https://blog.wire.com/blog/guest-rooms/) - users with premium accounts can create rooms that others can join without downloading anything or creating an account 

Wire uses open source software both client- and server-side. The servers are based in the European Union. According to Wire's [Privacy Whitepaper](https://wire-docs.wire.com/download/Wire+Privacy+Whitepaper.pdf) [PDF], Wire maintains the following metadata about conversations on the backend servers (this metadata is encrypted using transport encryption between the clients):

- Creator: The user who created the conversation.
- Timestamp: The UTC timestamp when the conversation was created.
- Participants list: The list of users who are participants of that conversation and their devices.
- Conversation name: Every user can name or rename a group conversation.

The [transparency report](https://wire-docs.wire.com/download/Wire+Transparency+Report.pdf) [PDF] shows that between 2014 and the end of 2017 Wire has not received any data handover requests.


#### Free and open source Self-Hosted/Federated Tools

##### Riot/Matrix

Riot is an open source client for the Matrix network, an open network for federated, end-to-end encrypted communication. Riot focuses on collaboration for teams and communities, and is available on web, desktop, iOS, and Android.

What makes this solution different is the concept of Homeservers. A Homeserver manages messages for users, recording them when they are received and providing them to users when they connect. Homeservers federate to communicate amongst each other. This means anyone can run a Homeserver and connect it to the greater network of Matrix, providing a distributed approach to building a chat network.

Features:

- Decentralised cryptographically signed conversation history replicated over all the servers that participate in a room
- Group and 1:1 Messaging
- End-to-end Encryption via the [Olm and Megolm double ratchet implementation](https://matrix.org/docs/guides/e2e_implementation.html)
- VoIP signalling for WebRTC
- Read receipts, Typing and Presence Notifications
- Server-side push notification rules
- Server-side search
- Synchronised read state and unread counts
- Decentralised content repository

- [Riot website](https://about.riot.im/)
- [Matrix website](https://matrix.org)
- [Matrix cryptographic review](https://www.nccgroup.trust/us/our-research/matrix-olm-cryptographic-review/)
- [Riot howto videos](https://about.riot.im/need-help/)


Riot can be a good solution for groups that want to self-host their internal chat instance. On the other hand, we must warn clients that it produces a lot of metadata, and, if their Matrix Homeserver is federated with others, this metadata will be shared with all the other servers, that clients cannot control.

It must also be noted that Riot/Matrix might not be the best solution for threat models where the state of Israel is considered one of the main adversaries, given that some of Matrix core developers work for a company that has tight links with Israel. For more information see [this Wikipedia page](https://en.wikipedia.org/wiki/Amdocs#Controversy).


##### XMPP

XMPP is an open communication protocol for instant messaging based on a federated model. Clients can create an account on one of the many available [XMPP servers](https://list.jabber.at/), or, if they're activists, can also choose to create an account on one of the Jabber/XMPP providers listed in this [page](https://riseup.net/en/security/resources/radical-servers).

Once a Jabber/XMPP account has been created, it can be used on several clients for desktop and mobile devices. XMPP conversations can be encrypted with [OTR](https://otr.cypherpunks.ca/) or [OMEMO](https://conversations.im/omemo/).

Since OTR does not allow for group encryption, and OMEMO's group encryption is [still unreliable](https://github.com/gkdr/lurch/issues/101), XMPP is only recommended for 1:1 conversations.


###### OMEMO

On mobile devices, it is best to use OMEMO, which allows for receiving messages that are sent while the user is offline.  This also means that if a malevolent actor has access to a user's device, they will be able to read all the messages that are sent to them.

- OMEMO [official page](https://conversations.im/omemo/)
- OMEMO [code on Github](https://github.com/omemo)

Features:

- Offline Messages / Backlog
- File Transfer
- Verifiability
- Deniability
- Forward Secrecy
- - Encrypted group chat (**please note that this feature is unreliable, especially if different clients are being used to access the group chat**)

The XMPP clients for mobile devices that allow for OMEMO encryption are **Conversations** and **Pix-Art Messenger** for Android and **ChatSecure** for iOS.

- Conversations can be downloaded from the [Play Store](https://play.google.com/store/apps/details?id=eu.siacs.conversations&amp;referrer=utm_source%3Dwebsite) or from [F-Droid](https://f-droid.org/en/packages/eu.siacs.conversations/).
    - [Official website](https://conversations.im)
    - [Code on Github](https://github.com/siacs/Conversations)
    - [Security specifications](https://conversations.im/#security)
    - On Conversations, messages can be set to disappear after a given time period for every conversation in the "Expert Settings" section of the settings:

        Settings -&gt; Advanced -&gt; Expert Settings -&gt; Automatic message deletion.

- Pix-Art Messenger is a fork of Conversations that allows both for OMEMO and OTR encryption. It can be downloaded from the [Play Store](https://play.google.com/store/apps/details?id=de.pixart.messenger) or from [F-Droid](https://f-droid.org/en/packages/de.pixart.messenger/), and the .apk file is also available on [Github](https://github.com/kriztan/Pix-Art-Messenger/releases/download/2.1.0/PixArtMessenger-2.1.0-standard-release.apk).
    - [Official website](https://jabber.pix-art.de/)
    - [Code on Github](https://github.com/kriztan/Pix-Art-Messenger)

- ChatSecure can be installed from the [App Store](https://itunes.apple.com/us/app/chatsecure/id464200063)
    - [Official website](https://chatsecure.org/)
    - [Code on Github](https://github.com/chatsecure)


###### OTR

If, given the user's threat model, it is more indicated to establish a different encrypted session for each conversation, and it is not crucial to encrypt group chats or receive messages that are sent to the user while they're offline, we should recommend the usage of OTR instead of OMEMO.

- OTR [official page](https://otr.cypherpunks.ca/)
- A git repository of the OTR source code can be found on the [otr.im community development site](https://otr.im/)

Features:
- Encryption of 1:1 messages
- Authentication of contacts
- Deniability
- Perfect forward secrecy


XMPP clients for mobile devices that allow for OTR encryption are **Xabber** and **Pix-Art Messenger** (see above) for Android and **ChatSecure** for iOS (see above).

- Xabber can be downloaded from the [Play Store](https://play.google.com/store/apps/details?id=com.xabber.android) or from [F-Droid](https://f-droid.org/packages/com.xabber.androiddev/).
    - [Official website](https://www.xabber.com)
    - [Code on Github](https://github.com/redsolution/xabber-android)



##### Mattermost

Mattermost is an open source, self-hosted Slack-alternative. It is a useful tool for team communications, that can be searched, accessed from various platforms, and connected to [other services](https://about.mattermost.com/community-applications/), like Gitlab, monitoring systems, or [Request Tracker](https://github.com/mmasquelin/RT-Extension-Mattermost).

Mattermost conversations are not end-to-end encrypted, but a group can self-host their instance behind a VPN.

Features:
- 1-1 and group messaging
- File sharing
- Webhooks and commands
- Threaded conversations

- The Android client can be installed from the [Play Store](https://play.google.com/store/apps/details?id=com.mattermost.rn) or from [F-Droid](https://f-droid.org/wiki/page/com.mattermost.mattermost)
- the iOS client can be installed from the [App Store](https://itunes.apple.com/us/app/mattermost/id1257222717?mt=8)

- [Official website](https://mattermost.com/)
- [Code on Gitlab.com](https://gitlab.com/gitlab-org/gitlab-mattermost)
- [How to install a Mattermost server](https://docs.mattermost.com/guides/administrator.html#install-guides)


#### Proprietary tools

##### Threema

Threema is a proprietary end-to-end encrypted instant messaging application for iOS, Android, and Windows Phone. It uses the open source NaCl library for encryption, which is open to independent audits. For more information on Threema's security, see [this page](https://threema.ch/en/faq/why_secure).

Features:
- text and voice messages
- voice calls
- File sharing
- Polls
- Silently agree or disagree to received messages
- Hide confidential chats and password-protect them with a PIN or fingerprint
- Can be used on tablets and devices without SIM card
- Verify your contacts via QR Code
- distribution lists
- no phone number required
- Optional contact sync
- Quote text messages
- Located in Switzerland


##### WhatsApp

Although WhatsApp is not open source, it is a very popular app and offers end-to-end encryption based on the Signal protocol.

The app can be configured to improve its security and privacy settings. To reduce risk and increase privacy when using WhatsApp, we should recommend the client to configure the following settings:

- Settings -&gt; Account -&gt; Privacy

    Last Seen: My Contacts

    Profile Photo: My Contacts

    Status: My Contacts

    Read Receipts: OFF

- Settings -&gt; Account -&gt; Security

    Show Security Notifications: ON

- Settings -&gt; Chats

    Save Incoming Media: OFF

    Chat Backup -&gt; Auto Backup: OFF

- Settings &gt;&gt; Notifications

    Show Preview: OFF (unfortunately, this still displays the sender’s name)

These settings are a reasonable middle ground for reducing the amount of data that the app creates locally, and minimizing what is exposed as clear text on iCloud/Google Drive.

WhatsApp can be downloaded from the [Play Store](https://play.google.com/store/apps/details?id=com.whatsapp) for Android and from the [App Store](https://itunes.apple.com/us/app/whatsapp-messenger/id310633997?mt%3D8) for iOS.
    - [WhatsApp website](https://www.whatsapp.com/)
    - [the grugq, Operational WhatsApp (on iOS)](https://medium.com/@thegrugq/operational-whatsapp-on-ios-ce9a4231a034#.rrfyxyjgc)


##### Facebook Messenger

Facebook Messenger is a messaging app and platform with standalone iOS and Android apps.

Facebook Messenger can also be used for end-to-end encrypted communications, but this feature is not the default. Secret messages can be used for sending messages, pictures, stickers, videos, and voice messages. It cannot be used for group messages and voice or video calls.

- To start a so called Secret Conversation, the client can follow [there instructions](https://www.facebook.com/help/messenger-app/811527538946901). 
- [More information on Secret Conversations](https://www.facebook.com/help/messenger-app/1084673321594605/)


##### Wickr

Wickr is a freemium proprietary instant messaging platform that allows users to exchange end-to-end encrypted and content-expiring messages, including photos, videos, and file attachments and place end-to-end encrypted video conference calls. The software is available for the iOS, Android, Mac, Windows 10, and Linux operating systems. The company is located in the US.

Features:
- end-to-end encrypted messages
- contacts verification
- forward secrecy
- registration without phone number or email

- [Wickr white paper](https://wickr.com/wickrs-messaging-protocol/)
- [open source cryptographic protocol](https://github.com/WickrInc/wickr-crypto-c)

If the client's main requirements is anonymous set-up (i.e. no phone number or email account used in setup) and they require self-destructing messages, we can recommend Wickr as the most suitable app for their needs.

* * *


| **Tool** | **Open source** | **Default E2E** | **Group chat** |**Encryption protocol** | **Anonymous sign-up** | **Email sign-up** | **Phone sign-up** | **Self-destructing messages** | **Remote message deletion** | **Metadata Collection** | **Paid** | **Jurisdiction** | **Self-hosted** | **Usability** |
|------|------|------|------|------|------|------|------|------|------|------|------|------|------|------|
| **Signal** | Yes | Yes | Yes | Signal | No | No | Yes | Yes | No | No | No | USA | No | Good |
| **Wire** | Yes | Yes | Yes | Proteus | No | Yes | Yes | Yes | Yes | Yes | Freemium | Switzerland | No | Good |
| **Riot** | Yes | Yes | Yes | Matrix | Yes | Yes | Yes | No | No | Yes | No | - | Yes | Medium |
| **XMPP+OMEMO** | Yes | No | Unreliable | OMEMO | No | Yes | No | Conversations: yes | No | Depends on the provider | No | - | Yes | Low |
| **XMPP+OTR** | Yes | No | No | OTR | No | Yes | No | No | No | Depends on the provider | No | - | Yes | Low |
| **Mattermost** | Yes | No | Yes | No encryption | No | Yes | No | No | Yes | No | No | - | Yes | Good |
| **Threema** | Only encryption protocol | Yes | Yes | NaCl | Yes | No | No | No | No | No | No | Switzerland | No | Good |
| **WhatsApp** | Only encryption protocol | Yes | Yes | Signal | No | No | Yes | Yes | Yes | Yes | No | USA | No | Good |
| **FB Messenger** | Only encryption protocol | No | No | Signal | No | Yes | No | Yes | No | Yes | No | USA | No | Good |
| **Wickr Me** | Only encryption protocol | Yes | Yes | Wickr | Yes | No | No | Yes | Yes | No | No | USA | No | Good |


* * *


### Comments


* * *


### Related Articles
