---
title: Initial Reply - For French Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for French speakers"
sidebar: mydoc_sidebar
permalink: 273-Initial_Reply_For_French_Speakerss.html
folder: mydoc
conf: Public
lang: fr
---


# Initial Reply
## First response, Email to Client for French speakers

### Body

Che(è)r(e) $ClientName,

Merci d'avoir contacté notre équipe d'aide aux activistes et aux journalistes pour les incidents de sécurité informatique (https://www.accessnow.org/plateforme-dassistance-pour-la-securite-numerique/), mon nom est $IHName et je suis là pour vous aider.

Je confirme qu'on a reçu votre requete et que notre équipe travaille dessus. 

Sachez que notre équipe est composée d'un groupe d'expertes et d'experts en sécurité numérique qui travaillent sur différents fuseaux horraires. Il peut arriver que vous recevez un e-mail d'un autre membre de notre équipe.
Si nous avons besoin de plus d'informations, nous vous enverrons un e-mail avec quelques questions dans peu de temps.

Veuillez garder dans le champ du sujet de nos e-mails le tag suivant “[accessnow #ID]” : c'est un identifiant qui nous sert à retracer l'historique de nos emails avec vous, concernant cet incident. 

Bien à vous,
$IHName


* * *


### Related Articles

- [Article #17: Initial Reply in English](17-Initial_Reply.html)
- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
