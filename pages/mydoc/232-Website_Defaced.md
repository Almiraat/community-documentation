---
title: Website Defaced
keywords: website, defacement
last_updated: July 19, 2018
tags: [defacement, articles]
summary: "The website has an inappropriate index page, or the content of any of its pages has changed, generally to launch a message opposite to the one supported by the authors of the site"
sidebar: mydoc_sidebar
permalink: 232-Website_Defaced.html
folder: mydoc
conf: Public
lang: en
---


# Website Defaced
## How to deal with a case of website defacement

### Problem

Most of the time defacement attacks tend to just change the appearance of the website, but in some cases the whole website can be damaged.

Sometimes the attackers try to impersonate the owner of the website to broadcast a different message or mislead supporters and/or followers.


### Solution

- In the first stage, we should not ask the client to provide any kind of access credentials and the right approach is to suggest them to follow the below instructions without our direct intervention. In case we need access to the website, we should make sure that the credentials are transmitted via a secure channel (mainly PGP or Signal). 

- Documenting the case is important:

    It is important to document every step we take in the ticket, so the work can be easily handed over to another team or partner. 

#### Steps

1. Instruct the client to enable the “Under Construction” mode. Most of the host providers and website management panels like CPanel for Linux hosting include such a feature. This feature should restrict any interaction with the website from the public side and allow the administrators to focus on the next steps without worrying that the website might suffer further damage.

2. Make sure that the website contents have been backed up, and that logs are being kept. This data will help later in case we need to identify the weakness that was exploited to change the appearance of the website and to make sure there has been no further unseen damage.

3. Check whether the client or the hosting provider have saved a recent on- or off-line backup. Instruct the client to restore the site by using this backup. This can be done through the control panel or by submitting a restore request to the hosting provider.

4. If the website is based on a known CMS such as WordPress or Drupal, make sure that the website is using the latest version, and that the plugins and used theme are up-to-date. In most cases, the main cause of the defacement is that the CMS and/or plugins are not up-to-date and the hacker could use an unpatched flaw to change the website. 

5. In case the website is not based on a CMS, we need to check the weakness through the libraries and technology used and code review should be done. This could be outside our expertise and we can ask help from partners such as VirtualRoad and Equalit.ie.


### Comments

The case can be considered resolved once the above instructions have been followed successfully and the website is back online.

Another ticket should be created for the preventative measures and website hardening. Article #30 suggest some good resources to harden CMS-based websites, and article #31 is a template we can use to recommend preventative measures to the owner of the website. The preventative measures should start by changing all access credentials (control panel, SSH, FTP, etc.).
