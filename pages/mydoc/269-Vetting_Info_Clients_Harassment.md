---
title: Inform Clients Targeted by Harassment about Vetting Process
keywords: email templates, vetting, vetting process, harassment, gendered online violence
last_updated: July 20, 2018
tags: [helpline_procedures_templates, harassment_templates, templates]
summary: "Template to inform clients who have been targeted by gendered online violence about our vetting process"
sidebar: mydoc_sidebar
permalink: 269-Vetting_Info_Clients_Harassment.html
folder: mydoc
conf: Public
lang: en
---


# Inform Clients Targeted by Harassment about Vetting Process
## Template to inform clients who have been targeted by gendered online violence about our vetting process

### Body

Hi [CLIENT_NAME],

We appreciate your trust in our Helpline service. We hope this message finds you well.
In order to protect our users and extend our trust network, we need to know more about you and ascertain who you are by referencing you through one of Access' Now partners.

We will reach out in a secure way and share your name and email to authenticate your identity and verify that you are indeed a member of civil society. Any other information, including the reason you contacted our Helpline, will remain strictly confidential.

To verify your identity, we are thinking of reaching out to the following organizations: [Vettor's Org Name - but not the vettor's name!] and [Vettor's Org Name - but not the vettor's name!]. Please let us know if you have reasons to believe that this might not be the best choice for your situation.

Please also note that Access Now’s Helpline services are not intended for individuals under the age of 18. If you are underage, please do let us know, and we will direct you to partners that can provide the support your require.

Our team will continue to work on your request while we complete this process. Please note that this process will be launched in 48 hours if you don't raise any objection.

Regards,
[YOUR_NAME]


* * *


### Comments

Please remember not to mention the name of the individual vettor we are going to reach out to.
