---
title: Secure Survey Tools
keywords: surveys, forms, LimeSurvey, Survey Monkey, Google Forms
last_updated: July 23, 2018
tags: [secure_communications, articles]
summary: "A client has requested advice on a secure survey tool"
sidebar: mydoc_sidebar
permalink: 284-Secure_survey_tools.html
folder: mydoc
conf: Public
lang: en
---


# Secure Survey Tools
## Secure alternatives to commercial survey forms

### Problem

A client is concerned about the current survey tool used by their organization and is requesting our advice on a secure, alternative survey tool.


* * *


### Solution

#### Questions to understand the client's context and needs:

- Where are you based?
- What survey tool are you currently using? Does this solution have any special feature you need?
- Is the form sent via email or is it posted in a website or social media page?
- Do you need to create complex surveys, or do you just need to make decisions on dates and on single questions?
- Do you have your own server? Would you prefer an option hosted on your server or a third-party solution?
- Is your main concern related to how the data is collected and transferred, or are you worried about how it is stored as well? Are  you worried about privacy, data leakage, compliance with standards or norms?
- Have you experience any kind of attack related to the current survey mechanism used? A suspicious message? An attempt to compromise accounts?
- Is there any standard or regulation the organization should comply with (GDRP, PCI, HIPAA, etc.)?


#### Recommended open source tools

- [**LimeSurvey**](http://limesurvey.org/)

    LimeSurvey is a fully-featured open source survey tool. It can be a bit complex to learn how to manage it, but their official documentation is pretty complete. It can be self-hosted or surveys can be created in LimeSurvey's own platform by [registering an account](https://www.limesurvey.org/sign-up). When setting up a survey on LimeSurvey's platform, users can choose to host it in Germany, Canada, USA, Australia, or the United Kingdom. 

    - [LimeSurvey user guide](https://manual.limesurvey.org/)
    - [Video tutorial: Create An Effective Online Survey and Questionnaire in LimeSurvey](https://youtu.be/ndpEhC6mysM)
    - If the client is using LimeSurvey's own platform, they can use [templates](https://www.limesurvey.org/examples) to prepare their first survey.

    If the client owns a server and has the capacity to manage it, they can install and set up LimeSurvey in their own server. This is a little more complex to set up, but will give them the benefit of having full control of the data and how it is handled. This solution includes some further security settings that can be useful: for example, the software can run within a docker container to isolate the instance, or results can be saved in an external upload directory.

    - [Instructions for installing a self-hosted instance of LimeSurvey](https://manual.limesurvey.org/Installation_-_LimeSurvey_CE)
    - More information on the docker image can be found [here](https://github.com/crramirez/limesurvey/)


##### Tools for simple polls

- [**Framadate**](https://git.framasoft.org/framasoft/framadate)

    Framadate is a free and open source tool for planning appointments or making decisions on a single topic. It is developed by the French free software association [Framasoft](https://framasoft.org/). No registration is required. Framadate can be self-hosted or used on one of the self-managed platforms that host it.

    - [Framasoft's platform](https://framadate.org/) (France)
    - [Disroot's platform](https://disroot.org/en/services/polls) (Netherlands)
    - [Instructions for self-hosting](https://framagit.org/framasoft/framadate/wikis/home)
   - Framadate can also be run as an [app within Sandstorm](https://apps.sandstorm.io/app/s244puc94dz2nph0n38qgkxkg3yrckxc93vxuz31grtey4rke3j0) (see [Article #282: Recommendations on Secure File Sharing and File Storage](282-Secure_file_sharing_storage.html) for more details on Sandstorm).

- [**Croodle**](https://github.com/jelhan/croodle)

    Croodle is a web application to schedule a date or to do a poll on a single general topic. Stored content data like title and description, number and labels of options and available answers and names of users and their selections is encrypted/decrypted in the browser using 256 bits AES. Croodle can be self-hosted or used on [Systemli's](https://www.systemli.org) platform.

    - [Systemli's Croodle](https://www.systemli.org/en/service/croodle.html)
    - [Instructions for self-hosting](https://github.com/jelhan/croodle#build-process-and-installation)


#### Commercial tools

**NOTE** Regarding other commercial tools, please explain to the client the implications of using a commercial service:

- The data will be saved in a server owned by a third party who needs to comply with the authorities.
- The implications for the data retention and privacy policy.
- How the data is stored and handled (is it encrypted? Who has access to the data?).

- [**Survey Monkey**](www.surveymonkey.com)

    Using an online service like Survey Monkey is fine as long as the client is aware of the amount of trust they are putting on them. It is important to review the privacy and data retention [policies](https://www.surveymonkey.com/mp/policy/privacy-policy/). Overall, Survey Monkey is serious about privacy and is not selling data to third parties, but they still own the data and will hand over this data to the police if they receive a subpoena.
        
    In order to get information about how the data is stored and handled, recommend that the client check Survey Monkey's [security statement](https://www.surveymonkey.com/mp/legal/security/).

- [**Google Forms**](https://www.google.com/forms/about/)

    If the client is using Google Forms, we should raise the same concerns as with any other commercial service regarding the risks connected to the storage of data in third-party servers and the obligation for the company to hand over data to authorities in case they receive official requests by the police or other authorities.

    - [Google Privacy Policy](https://policies.google.com/privacy?hl=en)


* * *


### Comments

#### References

- [The Best Online Survey Tools of 2018](https://www.pcmag.com/article2/0,2817,2494737,00.asp)


* * *


### Related Articles

- [Article #282: Recommendations on Secure File Sharing and File Storage](282-Secure_file_sharing_storage.html)
