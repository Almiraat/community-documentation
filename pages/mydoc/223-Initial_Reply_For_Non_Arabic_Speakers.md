---
title: Initial Reply - For Non-Arabic Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Non-Arabic speakers"
sidebar: mydoc_sidebar
permalink: 223-Initial_Reply_For_Non_Arabic_Speakers.html
folder: mydoc
conf: Public
lang: ar
---

أهلا بك،

أنا [Your Name] 
عضو من فريق مساعدي الأمان الرقمي لأكساس ناو.
https://www.accessnow.org/مساعدو-الأمان-الرقمي
 
لقد تلقينا البريد الإلكتروني الذي بعثته 
[Email Subject]. 
مع الأسف لن أستطيع التواصل معك بالعربية 
في صورة ما إذا تتطلب الوضعية التدخل السريع، الرجاء الإجابة و إضافة كلمة “URGENT” إلى موضوع البريد الإلكتروني. 

استطيع التواصل بالإنجليزية معك الآن : أنا موجود للدردشة على
 [xxxx@accessnow.org].
كما استطيع أن أرد عليك بالإنجليزية عبر الإيمايل
سيكون زملاؤنا الذين يتكلمون العربية على الخط بعد سويعات 

شكراً .
[Your name]


* * *


### Related Articles

- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
