---
title: PGP - Extend Expiration Date - Confirmation Email 
keywords: email, PGP, key management, expiration date, Enigmail, Thunderbird, email security
last_updated: November 6, 2018
tags: [secure_communications_templates, templates]
summary: "Email to Client to confirm that their PGP key has an extended expiration date"
sidebar: mydoc_sidebar
permalink: 40-PGP_Extend_Expiration_Confirmation_Email.html
folder: mydoc
conf: Public
lang: en
---


# PGP - Extend Expiration Date - Confirmation Email
## Email to Client to confirm that their PGP key has an extended expiration date

### Body

Hi [Name],

Thanks for reply. I just checked the new configuration of your PGP key and it is fine. 

Also I updated your key in all our systems, so your communications with the Helpline [and with the lists you have subscribed to] are secured.

I will now proceed to close this case, but please don't hesitate to get in touch if you need any more help. 

Regards,

[IH's Name]


* * *


### Related Articles

- [Article #37: PGP - Extend Expiration Date](37-PGP_Extend_Expiration.html)
