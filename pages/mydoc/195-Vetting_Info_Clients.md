---
title: Inform Clients of Vetting Process
keywords: email templates, vetting, vetting process
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "Template to inform clients about our vetting process"
sidebar: mydoc_sidebar
permalink: 195-Vetting_Info_Clients.html
folder: mydoc
conf: Public
lang: en
---


# Inform Clients of Vetting Process
## Template to inform clients about our vetting process

### Body

Hi [CLIENT_NAME],

We appreciate your trust in our Helpline service. In order to protect the at-risk users we serve and extend our trust network, we vet potential Helpline clients.

During this process, we reach out to Access Now's trusted partners where we may share your name and email to authenticate you as a member of civil society. Any other information, including the reason you contacted our Helpline, will remain  strictly confidential.

Please also note that Access Now’s Helpline services are not intended for individuals under the age of 18. If you are underage, please do let us know, and we will direct you to partners that can provide the support your require.

Our team will continue to work on your request while we complete this process. Please note that this vetting process will be launched in 48 hours if you don't raise any objection.

Regards,
[YOUR_NAME]


* * *


### Comments

For cases of gendered online violence, please use the template in [Article #269: Inform Clients Targeted by Harassment about Vetting Process](269-Vetting_Info_Clients_Harassment.html)


* * *


### Related Articles

- [Article #269: Inform Clients Targeted by Harassment about Vetting Process](269-Vetting_Info_Clients_Harassment.html)
