---
title: PGP - add User ID - email template
keywords: email, PGP, user ID, key management, Enigmail, Thunderbird, email security
last_updated: December 3, 2018
tags: [secure_communications_templates, templates]
summary: "Email to client to explain how to add a user ID to their PGP key"
sidebar: mydoc_sidebar
permalink: 9-PGP_add_userid_email.html
folder: mydoc
conf: Public
lang: en
---


# PGP - add User ID - email template
## Email to client to explain how to add a user ID to their PGP key

### Body

Dear [Name],

The process to add a new user ID to your PGP key involves: searching your key pair in the OpenPGP Key Management program, adding your new ID to your key pair, and finally publishing the updated public key on the key servers.

In order to do so, you can follow these instructions:

1. In Thunderbird, click the options menu and select Enigmail -&gt; Key Management.

2. In the "Search for" box type the email address you have already associated your PGP key with.

3. Right-click your PGP key and select "Manage User IDs".

4. Click the "Add" button.

5. Type your name and your new email address you want to associate your PGP key with. Then click OK. A pop-up window will appear - enter the passphrase for your secret PGP key.

6. Right-click your PGP key and select "Upload Public Keys to Keyserver".

7. Reply to this email including your new ID. Please, remember to add help@accessnow.org in CC and to keep [accessnow #] in the subject line.

If you have any questions or need more guidance, please send me an email at [UserName]@accessnow.org.

Best,

[IH's Name]



* * *


### Related Articles

- [Article #19: PGP - Add UserID](19-PGP_add_userID.html)
