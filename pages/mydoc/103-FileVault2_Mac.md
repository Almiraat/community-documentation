---
title: Instructions to set Up FileVault 2 on a Mac
keywords: include RT topic
last_updated: Month XX, 20XX
tags: [devices_data_security_templates, templates]
summary: "Email template to guide clients into setting up FileVault 2 in their Mac"
sidebar: mydoc_sidebar
permalink: 103-FileVault2_Mac.html
folder: mydoc
conf: Public
lang: en
---


# Instructions to set Up FileVault 2 on a Mac
## Email template to guide clients into setting up FileVault 2 in their Mac 

### Body

Hi [Client's Name],

I'm writing to send instructions on how to set up FileVault 2 in your computer. Please follow these steps and let me know if you have any questions.

1. Create a recovery/backup of your data to avoid any risk of data loss: 

    Prepare an external hard drive to store the backup. To backup your files, you can us Time Machine or an OS X Server from your network.

- You can find instructions on how to back up your files on an external hard drive with Time Machine here: https://support.apple.com/en-us/HT201250

- You can find instructions on how to set up Time Capsule on a local network for the first time here: https://support.apple.com/en-us/HT201510

    Please note that the first backup may take some time

2. Turn on FileVault by following these instructions: http://support.apple.com/en-us/HT4790

    When configuring FileVault, you can choose between two options:

    - Allow my iCloud to unlock my disk
    - Create a recovery key and do not use my iCloud account

    If you can keep your recovery key in a USB stick that you can keep in a safe place and not share with anyone else, the second option is safer.

Please keep in mind that full- disk encryption is only effective when the computer is powered off.

To learn more about full-disk encryption, you might want to read this guide: https://ssd.eff.org/en/module/keeping-your-data-safe
 
If you need more assistance, please let us know - we will be glad to assist you for any further need.

Best,

[IH's Name]


* * *


### Related Articles
