---
title: PGP - Transfer Key to New Machine - Email
keywords: email, PGP, user ID, key management, Enigmail, Thunderbird, email security
last_updated: November 6, 2018
tags: [secure_communications_templates, templates]
summary: "Email to client to explain how to transfer their PGP key pair to a new computer"
sidebar: mydoc_sidebar
permalink: 297-PGP_transfer_key_email.html
folder: mydoc
conf: Public
lang: en
---


# PGP - Transfer Key to New Machine - Email
## Email to client to explain how to transfer their PGP key pair to a new computer

### Body

Dear [Name],

The process to transfer your PGP key pair from your old to your new machine involves: (1) exporting your key pair from your old machine to a secure external storage device; (2) importing your key pair in your new machine; and (3) securely deleting your key pair from your old machine to make sure that no one who has access to your old machine also has access to your private key.

In order to do so, please follow these instructions:

#### In the old machine:

1. In Thunderbird, click the options menu and select "Enigmail" -&gt; "Key Management". 
2. In the "Search for" box type the email address you have already associated your PGP key with.
3. Right-click your PGP key and select "Export Keys to File".
4. In the prompt, click "Export Secret Keys" and choose to save the file in a secure external storage device, like a USB stick or an SD card.
5. If your revocation certificate is stored in your old computer, make sure to save it in an encrypted external storage device.


#### In the new machine:

1. Plug your external storage device into your new machine.
2. In Thunderbird, click the options menu and select "Enigmail" -&gt; "Key Management".
3. In the menu bar, click "File" -&gt; "Import Keys from File", then select the file with your PGP keys in your external drive.
4. Send me an encrypted email from your new computer, to check that everything works fine. I will then reply to check that you can decrypt your emails in your new machine.


[Only send the following instructions if the old machine is to be retired or passed to someone else]

#### Securely delete your revocation and secret PGP keys from your old machine:

1. Install in your old machine a tool to permanently delete those keys from your laptop:
    - [For Mac users] Download Permanent Eraser from this link: https://itunes.apple.com/fr/app/permanent-eraser/id500541921?mt=12
    - [For Windows users] Download EraserDrop Portable from this link: http://portableapps.com/apps/security/eraserdrop-portable
    - [For Linux users] Depending on what's installed in your computer, you can use the `wipe` or `shred` command line tools.

        For secure deletion of your entire hard disk, see this guide: https://wiki.archlinux.org/index.php/Securely_wipe_disk
        
    - Install the tool
    - Make a search for your secret key: in the search input add this pattern "*pub-sec.asc".
    - Use the secure data removal tool to delete the files and to delete your revocation key as well (make sure you have backed up your revocation key in a secure external storage device!).


Please let us know when you're done, or if you encounter any trouble, by replying to this email.

Best,

[IH's Name]



* * *


### Related Articles

- [Article #106: PGP - Transfer Key to New Machine](106-PGP_Transfer_Key_New_PC.html)
